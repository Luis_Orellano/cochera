/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cocheraluis.Cochera;

import static cocheraluis.Menu.LIBRE;
import static cocheraluis.Menu.RESET;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author Luis
 */
public class Posicion {
    
    private Scanner sc = new Scanner(System.in);
    private List<Plaza> plazas = new ArrayList<>();
    
    public Posicion(){
        
    }
    
    public void verPlazas() {
        boolean ok;

        System.out.println("Estas son las plazas actuales: ");
        this.getPlazas().forEach(System.out::println);

        do {
            try {
                ok = false;
                System.out.println("Quiere agregar una plaza nueva? 1-SI 2-NO");
                int respuesta = this.sc.nextInt();
                if (respuesta == 1) {
                    agregarPlaza();
                } else if (respuesta == 0) {
                    break;
                }
            } catch (Exception e) {
                System.out.println("Debe ingresar un valor numerico para continuar");
                this.sc.next();
                ok = true;
            }
        } while (ok);

    }
    
    private void agregarPlaza() {
        String nombre;
        boolean ok;

        do {
            try {
                ok = false;
                System.out.println("Ingrese un identificador para la plaza: ");
                nombre = this.sc.next().toUpperCase();

                for (Plaza p : this.getPlazas()) {
                    if (nombre.equals(p.getIdentificador())) {
                        System.out.println("Ese identificador ya pertenece a una plaza");
                        ok = true;
                    }
                }

                this.getPlazas().add(new Plaza(nombre, LIBRE + "Libre" + RESET, false));

            } catch (InputMismatchException e) {
                System.out.println("Debe ingresar un valor numerico para continuar");
                this.sc.next();
                ok = true;
            }

        } while (ok);

    }
    
    public void generarPlazas() {
        this.getPlazas().add(new Plaza("A", LIBRE + "LIBRE" + RESET, false));
        this.getPlazas().add(new Plaza("B", LIBRE + "LIBRE" + RESET, false));
        this.getPlazas().add(new Plaza("C", LIBRE + "LIBRE" + RESET, false));
        this.getPlazas().add(new Plaza("D", LIBRE + "LIBRE" + RESET, false));
        this.getPlazas().add(new Plaza("E", LIBRE + "LIBRE" + RESET, false));
        this.getPlazas().add(new Plaza("F", LIBRE + "LIBRE" + RESET, false));
        this.getPlazas().add(new Plaza("G", LIBRE + "LIBRE" + RESET, false));
        this.getPlazas().add(new Plaza("H", LIBRE + "LIBRE" + RESET, false));
        this.getPlazas().add(new Plaza("I", LIBRE + "LIBRE" + RESET, false));
        this.getPlazas().add(new Plaza("J", LIBRE + "LIBRE" + RESET, false));
        this.getPlazas().add(new Plaza("K", LIBRE + "LIBRE" + RESET, false));
        this.getPlazas().add(new Plaza("L", LIBRE + "LIBRE" + RESET, false));
    }

    public List<Plaza> getPlazas() {
        return plazas;
    }

    public void setPlazas(List<Plaza> plaza) {
        this.plazas = plaza;
    }    



    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.plazas);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Posicion other = (Posicion) obj;
        if (!Objects.equals(this.plazas, other.plazas)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Posicion{" + "posiciones=" + plazas + '}';
    }

    
    
    
}
