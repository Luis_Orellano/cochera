package cocheraluis;

import cocheraluis.Cochera.Plaza;

/**
 * Requerimientos Funcionales:
 *      Crear Planillas.
 *      Crear Registros.
 *      Visualizar planillas con registros.
 *      Consultar rapidamente espacios disponibles.
 *      Tener registro de la caja al comenzar el dia.
 *      Tener control de cierre y apertura de la cochera.
 * 
 * Requerimientos No Funcionales:
 *      Automatizacion para el espacio que ocupa cada vehiculo.
 *      Que identifique cada plaza con un color.
 *      La celda de efectivo registra automaticamente cada uno ingresado.
 *      Sumar caja final.
 *      Establecer horario de cerrar y abrir. 
 * 
 * @author Luis
 */

public class CocheraLuis {
    public static void main(String[] args) {
        System.out.println("\tBienvenido a la cochera ---ingresenombreaquí---");
        System.out.println("");
        System.out.println("Ingrese una opcion");

        Menu menu = Menu.getInstance();
        menu.initMenu();        
    }
}
