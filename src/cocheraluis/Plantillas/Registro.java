/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cocheraluis.Plantillas;

import cocheraluis.Cochera.Posicion;
import static cocheraluis.Menu.AUTO;
import static cocheraluis.Menu.LIBRE;
import static cocheraluis.Menu.MOTO;
import static cocheraluis.Menu.RESET;
import cocheraluis.Vehiculos.TipoVehiculo;
import cocheraluis.Vehiculos.Vehiculo;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author Luis
 */
public class Registro {
    private Scanner sc = new Scanner(System.in);
    private Vehiculo vehiculo;
    private String DNITitutar;
    private LocalTime ingreso;
    private String cochera; //posicion en la cochera ej A, CD, etc
    private LocalTime egreso;
    private LocalTime tiempoEstacionado;
    private float horasCobrar;
    private float valorHora;
    private float montoCobrar;

    public Registro() {
    }

    public Registro(
            Vehiculo vehiculo, String DNITitutar, LocalTime ingreso,
            String cochera, LocalTime egreso, LocalTime tiempoEstacionado, 
            float horasCobrar, float valorHora, float montoCobrar
    ) {
        this.vehiculo = vehiculo;
        this.DNITitutar = DNITitutar;
        this.ingreso = ingreso;
        this.cochera = cochera;
        this.egreso = egreso;
        this.tiempoEstacionado = tiempoEstacionado;
        this.horasCobrar = horasCobrar;
        this.valorHora = valorHora;
        this.montoCobrar = montoCobrar;
    }
    
    public Registro nuevoRegistro(Posicion posicion, Planilla planilla) {

        System.out.println("Que tipo de vehiculo es?");

        TipoVehiculo nuevoTipoVehiculo;
        Vehiculo nuevoVehiculo;
        Registro nuevoRegistro = null;

        String tipoVehiculo = this.sc.next();

        if (tipoVehiculo.equalsIgnoreCase("auto")) {
            System.out.println("Se registro un " + tipoVehiculo);
            nuevoTipoVehiculo = new TipoVehiculo(tipoVehiculo, 2);
            System.out.println("Ingrese la patente: ");
            String patente = this.sc.next();
            nuevoVehiculo = new Vehiculo(patente, nuevoTipoVehiculo);
            System.out.println("Ingrese su DNI: ");
            String dni = this.sc.next();
            nuevoRegistro = new Registro();
            nuevoRegistro.setVehiculo(nuevoVehiculo);
            nuevoRegistro.setDNITitutar(dni);
            nuevoRegistro.setIngreso(LocalTime.now());

            boolean ok;
            int iterator = 0;
            do {
                ok = false;
                if ((!posicion.getPlazas().get(iterator).isEstado()) && (!posicion.getPlazas().get(iterator).getColor().equalsIgnoreCase(LIBRE + "PARA MOTO" + RESET))) {
                    nuevoRegistro.setCochera(
                            posicion.getPlazas().get(iterator).getIdentificador()
                            + posicion.getPlazas().get(iterator + 1).getIdentificador());
                    posicion.getPlazas().get(iterator).setColor(AUTO + tipoVehiculo + RESET);
                    posicion.getPlazas().get(iterator + 1).setColor(AUTO + tipoVehiculo + RESET);
                    posicion.getPlazas().get(iterator).setEstado(true);
                    posicion.getPlazas().get(iterator + 1).setEstado(true);
                } else {
                    ok = true;
                }
                iterator++;
            } while (ok);
        } else if (tipoVehiculo.equalsIgnoreCase("moto")) {
            System.out.println("Se registro una " + tipoVehiculo);
            nuevoTipoVehiculo = new TipoVehiculo(tipoVehiculo, 1);
            System.out.println("Ingrese la patente: ");
            String patente = this.sc.next();
            nuevoVehiculo = new Vehiculo(patente, nuevoTipoVehiculo);
            System.out.println("Ingrese su DNI: ");
            String dni = this.sc.next();
            nuevoRegistro = new Registro();
            nuevoRegistro.setVehiculo(nuevoVehiculo);
            nuevoRegistro.setDNITitutar(dni);
            nuevoRegistro.setIngreso(LocalTime.now());

            boolean ok;
            int iterator = 0;
            do {
                ok = false;
                if ((!posicion.getPlazas().get(iterator).isEstado()) || (posicion.getPlazas().get(iterator).getColor().equalsIgnoreCase(LIBRE + "PARA MOTO" + RESET))) {
                    nuevoRegistro.setCochera(posicion.getPlazas().get(iterator).getIdentificador());
                    posicion.getPlazas().get(iterator).setColor(MOTO + tipoVehiculo + RESET);
                    if (iterator % 2 == 0) {
                        posicion.getPlazas().get(iterator + 1).setColor(LIBRE + "PARA MOTO" + RESET);
                    }
                    posicion.getPlazas().get(iterator).setEstado(true);
                } else {
                    ok = true;
                }
                iterator++;
            } while (ok);
        } else {
            System.out.println("De momento no se puede registrar ese vehiculo");
        }

        return nuevoRegistro;
    }
    
    public LocalTime difTiempo(LocalTime start, LocalTime end) {
        int hours = (int) ChronoUnit.HOURS.between(start, end);
        int minutes = (int) ChronoUnit.MINUTES.between(start, end);

        LocalTime difTime = LocalTime.of(hours, minutes);
        return difTime;
    }
    
    public int horasACobrar() {
        int horas = 0;

        if ((this.getTiempoEstacionado().getHour() > 0) && (this.getTiempoEstacionado().getMinute() > 45)) {
            horas = this.getTiempoEstacionado().getHour() + 1;
        } else if ((this.getTiempoEstacionado().getHour() > 0) && (this.getTiempoEstacionado().getMinute() < 45)) {
            horas = this.getTiempoEstacionado().getHour();
        } else {
            horas = 1;
        }

        return horas;
    }
    
    public int valorPorHora() {
        int valor = 0;

        if (this.getVehiculo().getTipoVehiculo().getNombre().equalsIgnoreCase("AUTO")) {
            valor = 100;
        } else {
            valor = 50;
        }

        return valor;
    }

    public float calcularMontoCobrar() {
        return this.getHorasCobrar() * this.getValorHora();
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getDNITitutar() {
        return DNITitutar;
    }

    public void setDNITitutar(String DNITitutar) {
        this.DNITitutar = DNITitutar;
    }

    public LocalTime getIngreso() {
        return ingreso;
    }

    public void setIngreso(LocalTime ingreso) {
        this.ingreso = ingreso;
    }

    public String getCochera() {
        return cochera;
    }

    public void setCochera(String cochera) {
        this.cochera = cochera;
    }

    public LocalTime getEgreso() {
        return egreso;
    }

    public void setEgreso(LocalTime egreso) {
        this.egreso = egreso;
    }

    public LocalTime getTiempoEstacionado() {
        return tiempoEstacionado;
    }

    public void setTiempoEstacionado(LocalTime tiempoEstacionado) {
        this.tiempoEstacionado = tiempoEstacionado;
    }

    public float getHorasCobrar() {
        return horasCobrar;
    }

    public void setHorasCobrar(float horasCobrar) {
        this.horasCobrar = horasCobrar;
    }

    public float getValorHora() {
        return valorHora;
    }

    public void setValorHora(float valorHora) {
        this.valorHora = valorHora;
    }

    public float getMontoCobrar() {
        return montoCobrar;
    }

    public void setMontoCobrar(float montoCobrar) {
        this.montoCobrar = montoCobrar;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.vehiculo);
        hash = 47 * hash + Objects.hashCode(this.DNITitutar);
        hash = 47 * hash + Objects.hashCode(this.ingreso);
        hash = 47 * hash + Objects.hashCode(this.cochera);
        hash = 47 * hash + Objects.hashCode(this.egreso);
        hash = 47 * hash + Objects.hashCode(this.tiempoEstacionado);
        hash = 47 * hash + Float.floatToIntBits(this.horasCobrar);
        hash = 47 * hash + Float.floatToIntBits(this.valorHora);
        hash = 47 * hash + Float.floatToIntBits(this.montoCobrar);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Registro other = (Registro) obj;
        if (Float.floatToIntBits(this.horasCobrar) != Float.floatToIntBits(other.horasCobrar)) {
            return false;
        }
        if (Float.floatToIntBits(this.valorHora) != Float.floatToIntBits(other.valorHora)) {
            return false;
        }
        if (Float.floatToIntBits(this.montoCobrar) != Float.floatToIntBits(other.montoCobrar)) {
            return false;
        }
        if (!Objects.equals(this.DNITitutar, other.DNITitutar)) {
            return false;
        }
        if (!Objects.equals(this.cochera, other.cochera)) {
            return false;
        }
        if (!Objects.equals(this.vehiculo, other.vehiculo)) {
            return false;
        }
        if (!Objects.equals(this.ingreso, other.ingreso)) {
            return false;
        }
        if (!Objects.equals(this.egreso, other.egreso)) {
            return false;
        }
        if (!Objects.equals(this.tiempoEstacionado, other.tiempoEstacionado)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Registro{" + "vehiculo=" + vehiculo + ", DNITitutar=" + DNITitutar + ", ingreso=" + ingreso + ", cochera=" + cochera + ", egreso=" + egreso + ", tiempoEstacionado=" + tiempoEstacionado + ", horasCobrar=" + horasCobrar + ", valorHora=" + valorHora + ", montoCobrar=" + montoCobrar + '}';
    }
    
}
