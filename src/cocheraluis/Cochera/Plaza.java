/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cocheraluis.Cochera;

import java.util.Objects;

/**
 *
 * @author Luis
 */
public class Plaza {
    private String identificador;
    private String color;
    private boolean estado; //false = desocupado o true = ocupado
    
    public Plaza(String identificador, String color, boolean estado){
        this.identificador = identificador;
        this.color = color;
        this.estado = estado;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.identificador);
        hash = 47 * hash + Objects.hashCode(this.color);
        hash = 47 * hash + (this.estado ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Plaza other = (Plaza) obj;
        if (this.estado != other.estado) {
            return false;
        }
        if (!Objects.equals(this.identificador, other.identificador)) {
            return false;
        }
        if (!Objects.equals(this.color, other.color)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Posicion{" + "identificador=" + identificador + ", color=" + color + ", estado=" + estado + '}';
    }
    
}
