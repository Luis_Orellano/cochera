/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cocheraluis.Caja;

import cocheraluis.Plantillas.Planilla;
import cocheraluis.Plantillas.Registro;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Luis
 */
public class Caja {

    private Scanner sc = new Scanner(System.in);
    private float cajaIncial;
    private float gastos;
    private float cadaDelDia;
    private float cajaFinal;

    public Caja() {
    }

    public void cajaInicial() {
        float inicial;
        boolean esNumerico;

        do {
            try {
                esNumerico = false;
                System.out.println("Ingrese la plata Inicial: ");
                inicial = this.sc.nextFloat();
                this.setCajaIncial(inicial);
                System.out.println("Tenes $" + this.getCajaIncial() + " en el inicio de caja");
            } catch (InputMismatchException ex) {
                System.out.println("Debe ingresar un valor numerico para continuar");
                this.sc.next();
                esNumerico = true;
            }
        } while (esNumerico);

    }

    public void cerrarCaja(List<Registro> registros) {
        try {
            registros.forEach(System.out::println);
            System.out.println("Ingrese los gastos del dia: ");
            this.gastos = this.sc.nextFloat();
            this.setGastos(this.gastos);
            this.setCadaDelDia(calcularCajaDelDia(registros));

            this.setCajaFinal(this.getCajaIncial() + this.getCadaDelDia() - this.getGastos());
            
           
        } catch (NullPointerException ex) {
            System.out.println("No iniciaste la caja del dia");
        }
    }

    private float calcularCajaDelDia(List<Registro> registros) {
        float total = 0;

        for (Registro reg : registros) {
            total += reg.getMontoCobrar();
        }

        return total;
    }

    public void verCaja(){
        System.out.println(this.toString());
    }

    public float getCajaIncial() {
        return cajaIncial;
    }

    public void setCajaIncial(float cajaIncial) {
        this.cajaIncial = cajaIncial;
    }

    public float getGastos() {
        return gastos;
    }

    public void setGastos(float gastos) {
        this.gastos = gastos;
    }

    public float getCadaDelDia() {
        return cadaDelDia;
    }

    public void setCadaDelDia(float cadaDelDia) {
        this.cadaDelDia = cadaDelDia;
    }

    public float getCajaFinal() {
        return cajaFinal;
    }

    public void setCajaFinal(float cajaFinal) {
        this.cajaFinal = cajaFinal;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Float.floatToIntBits(this.cajaIncial);
        hash = 97 * hash + Float.floatToIntBits(this.gastos);
        hash = 97 * hash + Float.floatToIntBits(this.cadaDelDia);
        hash = 97 * hash + Float.floatToIntBits(this.cajaFinal);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Caja other = (Caja) obj;
        if (Float.floatToIntBits(this.cajaIncial) != Float.floatToIntBits(other.cajaIncial)) {
            return false;
        }
        if (Float.floatToIntBits(this.gastos) != Float.floatToIntBits(other.gastos)) {
            return false;
        }
        if (Float.floatToIntBits(this.cadaDelDia) != Float.floatToIntBits(other.cadaDelDia)) {
            return false;
        }
        if (Float.floatToIntBits(this.cajaFinal) != Float.floatToIntBits(other.cajaFinal)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Caja{" + "cajaIncial=" + cajaIncial + ", gastos=" + gastos + ", cadaDelDia=" + cadaDelDia + ", cajaFinal=" + cajaFinal + '}';
    }

}
