/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cocheraluis.Vehiculos;

public class TipoVehiculo {
    String nombre;
    int cantidadPosicion;

    public TipoVehiculo() {
    }
    
    public TipoVehiculo(String nombre, int cantidadPosicion) {
        this.nombre = nombre;
        this.cantidadPosicion = cantidadPosicion;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidadPosicion() {
        return cantidadPosicion;
    }

    public void setCantidadPosicion(int cantidadPosicion) {
        this.cantidadPosicion = cantidadPosicion;
    }

    @Override
    public String toString() {
        return "TipoVehiculo{" + "nombre=" + nombre + ", cantidadPosicion=" + cantidadPosicion + '}';
    }
    
}
