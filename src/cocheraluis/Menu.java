/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cocheraluis;

import cocheraluis.Caja.Caja;
import cocheraluis.Cochera.Plaza;
import cocheraluis.Cochera.Posicion;
import cocheraluis.Vehiculos.TipoVehiculo;
import cocheraluis.Vehiculos.Vehiculo;
import cocheraluis.Plantillas.Planilla;
import cocheraluis.Plantillas.Registro;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Luis
 */
public final class Menu {

    public static final String AUTO = "\u001B[31m";
    public static final String LIBRE = "\u001B[32m";
    public static final String MOTO = "\u001B[33m";
    public static final String RESET = "\u001B[0m";

    private static Menu instance;
    private Planilla planilla = null;
    private Caja nuevaCaja = null;
    private Registro registro = null;
    private List<Registro> registros = new ArrayList<>();
    private Scanner sc = new Scanner(System.in);
    private Posicion posicion = null;
    private Vehiculo vehiculo = null;

    private Menu() {
        this.posicion = new Posicion();
        this.posicion.generarPlazas();
    }

    public static Menu getInstance() {
        if (instance == null) {
            instance = new Menu();
        }
        return instance;
    }

    public void initMenu() {

        int opc = 0;
        do {
            try {
                System.out.println("1- Crear Planilla del dia");
                System.out.println("2- Agregar registro a la planilla");
                System.out.println("3- Retirar Vehiculo");
                System.out.println("4- Ver Planilla del dia");
                System.out.println("5- Cerrar caja del dia");
                System.out.println("6- Ver caja del dia");
                System.out.println("7- Ver Plazas");
                System.out.println("0- Salir");

                opc = this.sc.nextInt();

                opciones(opc);
            } catch (InputMismatchException ex) {
                System.out.println("Debe ingresar una opcion valida para continuar");
                this.sc.next();
            }
        } while (opc > 0 || opc < 6);

    }

    private void opciones(int opc) {
        switch (opc) {
            case 1:
                try {
                    this.planilla = new Planilla().nuevaPlanilla();
                    this.nuevaCaja = new Caja();
                    this.nuevaCaja.cajaInicial();
                } catch (NullPointerException ex) {
                    System.out.println("No se inicio la caja o la planillaexit");
                }
                break;
            case 2:
                try {
                    this.registro = new Registro().nuevoRegistro(this.posicion, this.planilla);
                    this.registros.add(this.registro);
                    this.planilla.agregarRegistro(this.registro);
                } catch (NullPointerException e) {
                    System.out.println("No existe una planilla para ingresar registros o no se puede registrar");
                }
                break;
            case 3:
                if (this.registros.isEmpty()) {
                    System.out.println("No existen registros en la planilla");
                } else {
                    this.vehiculo = new Vehiculo();
                    this.vehiculo.retirarVehiculo(this.registros, this.posicion);
                }
                break;
            case 4:
                if (this.planilla == null || this.registros.isEmpty()) {
                    System.out.println("No existe planilla para mostrar o no existen registros");
                } else {
                    //this.registros.forEach(System.out::println);
                    this.registros.forEach(x -> System.out.println(x));
                }
                break;
            case 5:
                try {
                    this.nuevaCaja.cerrarCaja(this.registros);
                } catch (NullPointerException ex) {
                    System.out.println("no tenes registros para cerrar la caja");
                }
                break;
            case 6:
                try {
                    this.nuevaCaja.verCaja();
                } catch (NullPointerException ex){
                    System.out.println("No se creo la caja del dia");
                }
                break;
            case 7:
                this.posicion.verPlazas();
                break;
            case 0:
                salir();
                break;
            default:
                System.out.println("Opcion no valida");
                break;
        }
    }

    private void salir() {
        System.out.println("Saliendo del programa...");

        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            System.out.println(ex.getMessage());
        }

        System.exit(0);
    }
}
