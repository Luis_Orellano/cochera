package cocheraluis.Vehiculos;

import cocheraluis.Cochera.Plaza;
import cocheraluis.Cochera.Posicion;
import static cocheraluis.Menu.LIBRE;
import static cocheraluis.Menu.RESET;
import cocheraluis.Plantillas.Registro;
import java.time.LocalTime;
import java.util.List;
import java.util.Scanner;

public class Vehiculo {

    private Scanner sc = new Scanner(System.in);
    private String patente;
    private TipoVehiculo tipoVehiculo;

    public Vehiculo() {
    }

    public Vehiculo(String patente, TipoVehiculo tipoVehiculo) {
        this.patente = patente;
        this.tipoVehiculo = tipoVehiculo;
    }

    public void retirarVehiculo(List<Registro> registros, Posicion posicion) {
        System.out.println("Ingrese DNI del titular para retirar el vehiculo: ");
        String dni = this.sc.next();

        
        for (Registro reg : registros) {

            if ((dni.equals(reg.getDNITitutar())) ) {

                String letra[] = reg.getCochera().split("");
                for (int i = 0; i < letra.length; i++) {
                    System.out.println(letra[i]);
                    
                    for(Plaza p : posicion.getPlazas()){
                        if(p.getIdentificador().equals(letra[i]) || p.getColor().equalsIgnoreCase(LIBRE +"PARA MOTO" +RESET)){
                            p.setColor(LIBRE +"LIBRE"+RESET);
                            p.setEstado(false);
                        }
                    }
                }
                reg.setCochera(null);
                reg.setEgreso(LocalTime.now());
                LocalTime dif = reg.difTiempo(reg.getIngreso(), reg.getEgreso());
                reg.setTiempoEstacionado(dif);
                reg.setHorasCobrar(reg.horasACobrar());
                reg.setValorHora(reg.valorPorHora());
                reg.setMontoCobrar(reg.calcularMontoCobrar());
            }
           
        }
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public TipoVehiculo getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(TipoVehiculo tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    @Override
    public String toString() {
        return "Vehiculo{" + "patente=" + patente + ", tipoVehiculo=" + tipoVehiculo + '}';
    }

}
