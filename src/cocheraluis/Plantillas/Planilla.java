/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cocheraluis.Plantillas;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Planilla {
    List<Registro> registros = new ArrayList<>();
    
    public Planilla(){}

    private Planilla(String fecha) {
        System.out.println("Se creo la planilla del dia "+ fecha);
    }
    
    public Planilla nuevaPlanilla() {
        Planilla nuevaPlantilla = new Planilla(LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
        System.out.println("Planilla creada");
        return nuevaPlantilla;
    }
    
    public void agregarRegistro(Registro regNuevo) {
        this.registros.add(regNuevo);
    }
    
    public void eliminarRegistro(int index) {
        this.registros.remove(index);
    }

    public List<Registro> getRegistros() {
        return this.registros;
    }

    public void setRegistros(Registro registros) {
        this.registros.add(registros);
    }

    @Override
    public String toString() {
        return "Plantilla{" + "registros=" + registros + '}';
    }
    
}
